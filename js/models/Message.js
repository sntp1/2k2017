function Message(authorName, photoUrl, text, time) {
    var container = E('div', {className: 'message-container'}),
        nameHolder = E('span', {className: ''}).bindProperty(container, 'name', 'innerHTML', authorName),
        photoHolder = E('img', {className: 'img-circle message-photo'}).bindProperty(container, 'photoUrl', 'src', photoUrl),
        timeHolder = E('span', {className: 'padding-left'}).bindProperty(container, 'time', 'innerHTML', time),
        textHolder = E('p').bindProperty(container, 'text', 'innerHTML', text);

    return container
        .ac(photoHolder)
            .ac(E('div', {className: 'message'})
                .ac(nameHolder)
                .ac(timeHolder)
                .ac(textHolder));
}