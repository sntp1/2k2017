function Contact(name, photoUrl, phone) {
    var container = E('div', {COMPONENT_NAME: 'contact', className: 'list-group-item'}),
        nameHolder = E('p', {COMPONENT_NAME: 'name'}).bindProperty(container, 'name', 'innerHTML', name),
        photoHolder = E('img', {className: 'img-circle contact-photo'}).bindProperty(container, 'photoUrl', 'src', photoUrl),
        phoneHolder = E('p').bindProperty(container, 'phone', 'innerHTML', phone);

    container
        .ac(photoHolder)
        .ac(E('div', {className: 'contact-info'})
            .ac(nameHolder)
            .ac(phoneHolder));

    container.forwardEvent(container, 'click');

    return container;
}
