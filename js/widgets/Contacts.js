function Contacts() {
    var container = E('div', {
        COMPONENT_NAME: 'contacts',
        className: 'list-group'
    });

    container.getContacts = function () {
        return container.childNodes;
    };

    container.addContact = function (contact) {
        this.appendChild(contact);
    };

    return container;
}