function SendMessageWidget() {
    var container = E('div', {COMPONENT_NAME: 'sendMessageWidget', className: 'force-to-bottom col-md-7'}),
        textarea = E('textarea', {className: 'form-control'}).bindProperty(container, 'text', 'value'),
        button = E('button', {COMPONENT_NAME: 'sendButton', className: 'btn btn-primary send-button', innerHTML: 'Send'});

    container
        .ac(textarea)
        .ac(button);

    button.forwardEvent(container, 'click');

    return container;
}