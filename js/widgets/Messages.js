function Messages() {
    var container = E('div', {className: 'col-md-8'});
    container.bindProperty(container, 'currentCollocutor');

    container.getMessages = function () {
        return container.childNodes;
    };

    container.addMessage = function(message) {
        container.appendChild(message);
    };

    container.clear = function () {
        container.innerHTML = '';
    };

    return container;
}