function ChatWidget() {
    var container = E('div', {className: 'container'}),
        contacts = Contacts(),
        messages = Messages(),
        sendMessageWidget = SendMessageWidget();

    var contactsManager = new ContactsManager(),
        messagesManager = new MessageManager();

    container
        .ac(E('div', {className: 'raw'})
            .ac(E('div', {className: 'col-md-4'})
                .ac(contacts))
            .ac(E('div', {className: 'col-md-8'})
                .ac(messages)
                .ac(sendMessageWidget)));

    /* TODO: how to make onload event??? */
    var loadedContacts = contactsManager.loadContacts();
    for (var i in loadedContacts) {
        contacts.addContact(loadedContacts[i]);
    }

    container.handledEvents = {
        'contacts.contact.click': function(contact) {
            messages.clear();
            messages.setCurrentCollocutor(contact);

            var loadedMessages = messagesManager.loadMessages(contact.getName());
            for (var i in loadedMessages) {
                messages.addMessage(loadedMessages[i]);
            }
        },
        'sendMessageWidget.sendButton.click': function (widget) {
            var me = contactsManager.me();

            var date = new Date();
            var time = date.getDate() + '-' + date.getMonth() + '-' + date.getYear() + ', '
                + date.getHours() + ':' + date.getMinutes();

            var message = Message(me.getName(), me.getPhotoUrl(), widget.getText(), time);
            messagesManager.sendMessage(messages.getCurrentCollocutor().getName(), message);
            messages.addMessage(message);
            sendMessageWidget.setText('');
        }
    };

    return container;
}