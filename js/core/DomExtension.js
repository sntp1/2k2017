String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

Element.prototype.ac = function (node) {
    this.appendChild(node);
    return this;
};

Element.prototype.bindProperty = function(target, targetName, property, value) {
    if (property == undefined) {
        property = targetName;
    }
    var elem = this;

    target['get' + targetName.capitalize()] = function () {
        return elem[property];
    };
    target['set' + targetName.capitalize()] = function (val) {
        elem[property] = val;
    };

    if (value != undefined) {
        this[property] = value;
    }
    return this;
};

var E = function (tagName, attr) {
    var elem = document.createElement(tagName);
    if (attr != undefined) {
        for (var key in attr) {
            elem[key] = attr[key];
        }
    }
    return elem;
};

Element.prototype.handleChildEvent = function(target, event) {
    if (this.handledEvents && this.handledEvents[event]) {
        this.handledEvents[event](target);
    } else if (this.parentNode && this.parentNode.handleChildEvent) {
        if (this.COMPONENT_NAME) {
            event = this.COMPONENT_NAME + '.' + event;
        }
        this.parentNode.handleChildEvent(target, event);
    }
};

Element.prototype.forwardEvent = function (target, event) {
    this.addEventListener(event, function () {
        if (this.COMPONENT_NAME == undefined) {
            throw new Error('Property COMPONENT_NAME should be set in ' + this);
        }
        this.parentNode.handleChildEvent(target, this.COMPONENT_NAME + '.' + event);
    });
};
