class MessageManager {
    constructor() {
        this.messages = {
            Alex: [
                Message('Alex', './img/alex.jpg', 'Hey Slavik!', '20-12-16, 7:00 PM'),
                Message('Slavik', './img/slavik.jpg', 'Hi Alex, how are you?', '20-12-16, 7:01 PM'),
                Message('Alex', './img/alex.jpg', 'Fine. What are you doing?', '20-12-16, 7:02 PM'),
                Message('Slavik', './img/slavik.jpg', 'Killing React', '20-12-16, 7:02 PM'),
                Message('Alex', './img/alex.jpg', 'Oh boy...', '20-12-16, 7:03 PM')
            ],
            Actor: [
                Message('Actor', './img/actor.jpg', 'Smells like a shit', '21-12-16, 2:34 PM'),
                Message('Slavik', './img/slavik.jpg', 'Just making new js framework', '21-12-16, 3:15 PM'),
                Message('Actor', './img/actor.jpg', 'Wow! Js! I do love it', '21-12-16, 3:15 PM'),
            ]
        }
    }

    loadMessages(name) {
        return this.messages[name];
    }

    sendMessage(name, message) {
        this.messages[name].push(message);
    }
}