class ContactsManager {
    
    constructor() {
	   this.contacts = [
            Contact('Alex', './img/alex.jpg', '+79373475432'),
            Contact('Actor', './img/actor.jpg', '+77673432343')
       ];

	   this.slavik = Contact('Slavik', './img/slavik.jpg', '+79657873410');
    }

    loadContacts() {
        return this.contacts;
    }

    me() {
        return this.slavik;
    }
}